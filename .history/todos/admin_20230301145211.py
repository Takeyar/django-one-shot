from django.contrib import admin
from todos.models

@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    pass
