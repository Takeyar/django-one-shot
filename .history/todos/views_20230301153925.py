from django.shortcuts import render
from todos.models import TodoList


def todo_list_list(request):
    list = TodoList.object.all()
    context = {"todo_list"to": list}
    return render("todos/list.html", context)
