from django.shortcuts import render
from todos.models import TodoList


def todo_list_list(request):
    list = TodoList.object.all()
    context = {": list, }
    return render(request, "todos/list.html", context)
