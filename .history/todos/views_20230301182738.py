from django.shortcuts import render
from todos.models import TodoList


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {"lists": list, }
    return render(request, "todos/list.html", context)
