from django.shortcuts import render
from todos.models import TodoList


def Todolist(request):
    list = TodoList.object.all()
    context = {"todolist_list"ist": list,}
    return render(request,"todos/list.html", context)
