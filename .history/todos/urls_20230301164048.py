from django.contrib import admin
from django.urls import path
from todos.views import todo_list_list

urlpatterns = [
    path("", "to_do_list_list", name="todo_list_list")
]
