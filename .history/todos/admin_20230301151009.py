from django.contrib import admin
from todos.models import TodoList,TodoItem


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    pass


@admin.register(TodoItem)
class TodoItem
